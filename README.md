# DRM Video Player Debugger

> Right now only support Mac & IOS Safari

<https://drm-video-players-debugger.vercel.app/>

## Required

- Nodejs v16.13+
- [pnpm](https://pnpm.io/installation#using-corepack)

## Get Started

```sh
# install deps
pnpm i

# serve on local
pnpm dev
```

## Player Source Code

```sh
src/views/
├── BitmovinPlayerView.vue
├── ShakaPlayerView.vue
├── THEOPlayerView.vue
└── VideojsView.vue
```
