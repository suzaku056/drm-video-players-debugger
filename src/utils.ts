/* eslint-disable no-bitwise */
/* eslint-disable no-plusplus */
import axios from 'axios';

export async function createLicenseToken({
  videoUrl,
}: { videoUrl: string }): Promise<{ data: { token: string } }> {
  const contentId = new URL(videoUrl).pathname.slice(1).split('.')[0];

  const { data } = await axios.post(
    'https://5y2c8id2yk.execute-api.ap-northeast-1.amazonaws.com/stage/license-tokens',
    {
      contentId,
      userId: 506,
      drmType: 'FairPlay',
    },
  );
  console.log('license token: ', data.data.token);

  return data;
}

export function base64DecodeUint8Array(input: string) {
  const raw = window.atob(input);
  const rawLength = raw.length;
  const array = new Uint8Array(new ArrayBuffer(rawLength));

  for (let i = 0; i < rawLength; i++) {
    array[i] = raw.charCodeAt(i);
  }

  return array;
}

export function base64EncodeUint8Array(input: string) {
  const keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  let output = '';
  let chr1; let chr2; let chr3; let enc1; let enc2; let enc3; let
    enc4;
  let i = 0;

  while (i < input.length) {
    chr1 = input[i++];
    chr2 = i < input.length ? input[i++] : Number.NaN;
    chr3 = i < input.length ? input[i++] : Number.NaN;

    // @ts-ignore
    enc1 = chr1 >> 2;
    // @ts-ignore
    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
    // @ts-ignore
    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
    // @ts-ignore
    enc4 = chr3 & 63;

    // @ts-ignore
    // eslint-disable-next-line no-restricted-globals
    if (isNaN(chr2)) {
      // eslint-disable-next-line no-multi-assign
      enc3 = enc4 = 64;
      // @ts-ignore
      // eslint-disable-next-line no-restricted-globals
    } else if (isNaN(chr3)) {
      enc4 = 64;
    }
    output += keyStr.charAt(enc1) + keyStr.charAt(enc2)
      + keyStr.charAt(enc3) + keyStr.charAt(enc4);
  }
  return output;
}

export function arrayToString(array: any) {
  const uint16array = new Uint16Array(array.buffer);
  // @ts-ignore
  return String.fromCharCode.apply(null, uint16array);
}

export function extractContentId(initData: any) {
  const contentId = arrayToString(initData);
  return contentId.substring(contentId.indexOf('skd://') + 6);
}

export function prepareCertificate(rawResponse: any) {
  // @ts-ignore
  const responseText = String.fromCharCode.apply(null, new Uint8Array(rawResponse));
  const raw = window.atob(responseText);
  const rawLength = raw.length;
  const certificate = new Uint8Array(new ArrayBuffer(rawLength));
  for (let i = 0; i < rawLength; i++) {
    certificate[i] = raw.charCodeAt(i);
  }
  return certificate;
}
