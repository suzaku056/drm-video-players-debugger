export const LICENSE_URL = 'https://license.pallycon.com/ri/licenseManager.do';
export const FAIR_PLAY_CERT_URL = 'https://license-global.pallycon.com/ri/fpsKeyManager.do?siteId=YRV1';

export const DEFAULT_VIDEO_URL = 'https://d1uytmezvtfejn.cloudfront.net/J6nT4k15sNxjjuOL3tE8eQQMEm1fKhRZdafhwflv2aVk3FM5pF.m3u8';
// export const DEFAULT_VIDEO_URL = 'https://d1uytmezvtfejn.cloudfront.net/jdcwGwFkhZ8houCPxxKzu44IJ7vxXq7OykdBRPhkUKTO1jtjfz.m3u8';
