import { createRouter, createWebHistory } from 'vue-router';
import VideojsView from '../views/VideojsView.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'videojs',
      component: VideojsView,
    },
    {
      path: '/theoplayer',
      name: 'theoplayer',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('@/views/THEOPlayerView.vue'),
    },
    {
      path: '/shaka-player',
      name: 'shaka-player',
      component: () => import('@/views/ShakaPlayerView.vue'),
    },
    {
      path: '/bitmovin-player',
      name: 'bitmovin-player',
      component: () => import('@/views/BitmovinPlayerView.vue'),
    },
    {
      path: '/radiant-media-player',
      name: 'radiant-media-player',
      component: () => import('@/views/RadiantMediaPlayerView.vue'),
    },
  ],
});

export default router;
